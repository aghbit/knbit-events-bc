package knbit.events.bc.backlogevent.domain.valueobjects;

public enum BacklogEventState {
    ACTIVE, INACTIVE;
}
