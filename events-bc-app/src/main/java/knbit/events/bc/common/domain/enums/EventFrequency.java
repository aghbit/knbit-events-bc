package knbit.events.bc.common.domain.enums;

public enum EventFrequency {
    ONE_OFF, CYCLIC;
}
