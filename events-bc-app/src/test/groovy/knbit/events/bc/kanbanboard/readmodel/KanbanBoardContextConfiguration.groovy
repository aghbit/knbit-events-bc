package knbit.events.bc.kanbanboard.readmodel

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan

@ComponentScan(basePackages = "knbit.events.bc.kanbanboard")
@EnableAutoConfiguration
class KanbanBoardContextConfiguration {
}
